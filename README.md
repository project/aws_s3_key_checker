# AWS S3 Key Checker

Offers a form to check if a series of keys exists or not inside a AWS S3 bucket.

## INTRODUCTION ##
This module is useful to perform a batch operation to ensure a list of keys is
present or not inside a bucket.

It performs a HEAD operation to see if a key exists inside a AWS S3 bucket.
That key can be prefixed and/or suffixed with any string.

## REQUIREMENTS ##
* A working AWS S3 bucket
* A valid AWS account and credentials (access and secret key)
* [AWS SDK for PHP](https://aws.amazon.com/sdk-for-php/): installed by this
  module when using composer
* The following ACL permissions granted for the user performing the check:
  * READ

## INSTALLATION ##
Run `composer require drupal/aws_s3_key_checker`.

## CONFIGURATION ##
Configuration available at /admin/config/aws/s3/key-checker.
