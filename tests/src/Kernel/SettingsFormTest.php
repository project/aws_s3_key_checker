<?php

namespace Drupal\Tests\aws_s3_key_checker\Kernel;

use Drupal\aws_s3_key_checker\Form\SettingsForm;
use Drupal\Core\Form\FormState;
use Drupal\Core\Site\Settings;
use Drupal\KernelTests\KernelTestBase;

/**
 * Testing for settings form.
 *
 * @group user
 */
class SettingsFormTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system', 'aws_s3_key_checker'];

  /**
   * Submit the aws_s3_key_checker_settings form.
   */
  public function testConfigForm() {
    $form = SettingsForm::create($this->container);

    $form_state = new FormState();
    \Drupal::formBuilder()->submitForm($form, $form_state);
    $this->assertTrue(isset($form_state->getErrors()['credentials']), "An error must be thrown when no credentials are defined");

    new Settings([
      'aws_s3_key_checker.access_key' => $this->randomMachineName(),
      'aws_s3_key_checker.secret_key' => $this->randomMachineName(),
    ]);
    \Drupal::formBuilder()->submitForm($form, $form_state);
    $this->assertTrue(empty($form_state->getErrors()['credentials']), "No error must be thrown when credentials are defined");
    $this->assertTrue(isset($form_state->getErrors()['buckets']), "An error must be thrown when no buckets are defined");

    $form_state = (new FormState())->setValues(['buckets' => 'bucket-name']);
    \Drupal::formBuilder()->submitForm($form, $form_state);
    $this->assertTrue(str_starts_with($form_state->getErrors()['buckets'], 'No pipe separator found'), "An error must be thrown if bucket field does not contain a pipe separator");

    $bucketsRawValue = 'bucket-name1|region1\nbucket-name2|region2';
    $form_state = (new FormState())->setValues(['buckets' => $bucketsRawValue]);
    \Drupal::formBuilder()->submitForm($form, $form_state);
    $this->assertTrue(empty($form_state->getErrors()['buckets']), "No error must be thrown when bucket field format is correct");
    $this->assertTrue(empty($form_state->getErrors()), "No error must be thrown when credentials and buckets are defined");

    $bucketsExpectedValue = explode("\n", $bucketsRawValue);
    $this->assertEquals($this->config('aws_s3_key_checker.settings')->get('buckets'), $bucketsExpectedValue);
  }

}
