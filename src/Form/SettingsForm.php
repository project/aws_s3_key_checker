<?php

namespace Drupal\aws_s3_key_checker\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;

/**
 * Configuration form for AWS S3 Key Checker.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aws_s3_key_checker_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['aws_s3_key_checker.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('aws_s3_key_checker.settings');

    $form['credentials'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Amazon Web Services Credentials'),
      '#description' => $this->t(
        "To set access and secret key you must use \$settings['aws_s3_key_checker.access_key'] and \$settings['aws_s3_key_checker.secret_key'] in your site's settings.php file.<br/>Submitting this form without adding your credentials will throw an error unless you use IAM credentials."
      ),
    ];

    $form['use_iam_credentials'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use IAM credentials'),
      '#description' => $this->t("Use IAM credentials instead of the ones defined in your site's settings.php"),
      '#default_value' => $config->get('use_iam_credentials') ?? FALSE,
    ];

    $form['buckets'] = [
      '#type' => 'textarea',
      '#title' => $this->t('AWS S3 Buckets available to be checked'),
      '#description' => $this->t("Enter the buckets's name, a pipe (|) and it's region (e.g.- my-bucket-name|us-east-1). You can enter more than one bucket, one per line."),
      '#required' => TRUE,
      '#default_value' => $config->get('buckets') ? implode("\n", $config->get('buckets')) : NULL,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $useIamCredentials = (bool) $form_state->getValue('use_iam_credentials');
    $accessKey = Settings::get('aws_s3_key_checker.access_key');
    $secretKey = Settings::get('aws_s3_key_checker.secret_key');
    if (!$useIamCredentials && (!$accessKey || !$secretKey)) {
      $form_state->setErrorByName(
        'credentials',
        $this->t("No credentials found. You must define credentials in your site's settings.php file before submitting this form")
      );
    }
    $buckets = $this->cleanBuckets($form_state->getValue('buckets'));

    foreach ($buckets as $bucket) {
      if (strpos($bucket, '|') === FALSE) {
        $form_state->setErrorByName(
        'buckets',
        $this->t("No pipe separator found in one of your buckets. You must enter the buckets's name, a pipe (|) and it's region (e.g.- my-bucket-name|us-east-1)")
        );
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('aws_s3_key_checker.settings');
    $config
      ->set('use_iam_credentials', $form_state->getValue('use_iam_credentials'))
      ->set('buckets', $this->cleanBuckets($form_state->getValue('buckets')))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Clean the bucket's string and return an array.
   *
   * @param string $rawBuckets
   *   Raw bucket's string coming from config form.
   *
   * @return array
   *   Cleaned array of buckets
   */
  public function cleanBuckets(string $rawBuckets) {
    $buckets = explode("\n", $rawBuckets);
    $buckets = array_map('trim', $buckets);
    // Remove empty values.
    $buckets = array_filter($buckets);
    // Remove extra white space.
    return array_map(function ($bucket) {
      return preg_replace("/\s*(\S+)\s*\|\s*(\S+)\s*/", '$1|$2', $bucket);
    }, $buckets);
  }

}
