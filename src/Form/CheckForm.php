<?php

namespace Drupal\aws_s3_key_checker\Form;

use Aws\Credentials\Credentials;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;

/**
 * Configuration form for AWS S3 Key Checker.
 */
class CheckForm extends FormBase {
  protected const KEY_PRESENT = 1;
  protected const KEY_NOT_PRESENT = 0;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aws_s3_key_checker_check';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('aws_s3_key_checker.settings');

    $buckets = [];
    $configBuckets = $config->get('buckets');
    if (is_array($configBuckets)) {
      foreach ($configBuckets as $bucketLine) {
        [$bucket, $region] = explode('|', $bucketLine);
        $buckets[$bucketLine] = $bucket . ' (' . $region . ')';
      }
    }

    $form['buckets'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#multiple' => FALSE,
      '#title' => $this->t('Bucket'),
      '#options' => $buckets,
      '#description' => $this->t(
        'Select bucket to be checked. Configure this list in the <a href="@settings_url">settings</a> form.',
        [
          '@settings_url' => Url::fromRoute('aws_s3_key_checker.settings')
            ->toString(),
        ]
      ),
      '#default_value' => $buckets ? array_keys($buckets)[0] : NULL,
    ];

    $form['keys'] = [
      '#type' => 'textarea',
      '#required' => TRUE,
      '#title' => $this->t('Enter the keys to be checked'),
      '#description' => $this->t('One per line, with or without a leading slash.'),
    ];

    $form['policy'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#multiple' => FALSE,
      '#title' => $this->t('Check policy'),
      '#options' => [
        self::KEY_PRESENT => 'Key exists',
        self::KEY_NOT_PRESENT => 'Key does not exist',
      ],
      '#default_value' => self::KEY_PRESENT,
    ];

    $form['prefix'] = [
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => $this->t('Prefix'),
      '#description' => $this->t('Add a prefix to all keys'),
    ];

    $form['suffix'] = [
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => $this->t('Suffix'),
      '#description' => $this->t('Add a suffix to all keys'),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('aws_s3_key_checker.settings');
    [$bucket, $region] = explode('|', $form_state->getValue('buckets'));
    $rawKeys = explode("\n", $form_state->getValue('keys'));
    $keys = [];
    foreach ($rawKeys as $key) {
      $key = trim($key);
      if (empty($key)) {
        continue;
      }
      $key = $form_state->getValue('prefix') . $key . $form_state->getValue('suffix');
      $key = str_replace('//', '/', $key);
      $key = ltrim($key, '/');
      if ($key) {
        $keys[] = $key;
      }
    }
    $total = count($keys);

    $batch = [
      'title' => $this->t('Checking keys'),
      'operations' => [],
      'init_message' => $this->t('Checking process is starting.'),
      'progress_message' => $this->t('Processed @current out of @total. Estimated time: @estimate.'),
      'error_message' => $this->t('The process has encountered an error.'),
      'finished' => '\Drupal\aws_s3_key_checker\Form\CheckForm::checkFinishedCallback',
    ];

    foreach ($keys as $key) {
      $batch['operations'][] = [
        ['\Drupal\aws_s3_key_checker\Form\CheckForm', 'checkKey'],
        [
          $region,
          $bucket,
          $key,
          $form_state->getValue('policy'),
          $config->get('use_iam_credentials'),
        ],
      ];
    }

    batch_set($batch);

    $this->messenger()->addMessage('Checked ' . $total . ' keys');

  }

  /**
   * Checks if a key exists in a AWS S3 bucket.
   *
   * @param string $region
   *   AWS S3 region.
   * @param string $bucket
   *   Bucket name.
   * @param string $key
   *   Key to be checked.
   * @param int $policy
   *   Policy to use.
   * @param mixed $context
   *   Drupal's batch context.
   */
  public static function checkKey(string $region, string $bucket, string $key, int $policy, bool $useIamCredentials, &$context) {
    // Set up clients.
    $s3Client = new S3Client([
      'version' => 'latest',
      'region' => $region,
    ]);

    if (!$useIamCredentials) {
      $s3Client['credentials'] = new Credentials(
        Settings::get('aws_s3_key_checker.access_key'),
        Settings::get('aws_s3_key_checker.secret_key')
      );
    }

    $exists = FALSE;
    try {
      // Get the object.
      $result = $s3Client->headObject([
        'Bucket' => $bucket,
        'Key' => $key,
      ]);
      if ($result) {
        $exists = TRUE;
      }
    }
    catch (S3Exception $e) {
      $exists = FALSE;
    }

    if ($policy === self::KEY_PRESENT) {
      $context['results'][] = ['key' => $key, 'match' => $exists];
    }
    else {
      $context['results'][] = ['key' => $key, 'match' => !$exists];
    }
  }

  /**
   * Called when batch is finished.
   *
   * @param bool $success
   *   TRUE if no fatal PHP errors were detected.
   * @param array $results
   *   Array with results of processing batch.
   */
  public static function checkFinishedCallback(bool $success, array $results): void {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $matches = [];
      foreach ($results as $result) {
        if ($result['match']) {
          $matches[] = $result['key'];
        }
      }
      if (count($matches) > 0) {
        $message = \Drupal::translation()
          ->translate('The following keys matched your criteria:') . '<ul/><li>';
        $message .= implode('</li><li>', $matches);
        $message .= '</li></ul>';
        $message = Markup::create($message);
        \Drupal::messenger()->addMessage($message);
      }
      else {
        $message = \Drupal::translation()
          ->translate('No keys matched your criteria');
        \Drupal::messenger()->addMessage($message);
      }
    }
    else {
      $message = t('Finished with an error.');
      \Drupal::messenger()->addError($message);
    }

  }

}
